using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class LoginUser : MonoBehaviour
{

    public Text UserNameText;
    public Text PasswordText;
    public bool exist;

    public GameObject SherUser; 

    public void GetUserData()
    {
        //PlayerPrefs.DeleteAll();
        exist = false;
        Debug.Log("Solicitando...");

        FirebaseDatabase
            .DefaultInstance
            .GetReference("users")
            .GetValueAsync()
            .ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted)
                {
                    // Handle the error...
                    Debug.Log("Error");
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    // Iterate each users in snapshot.Children
                    foreach (DataSnapshot user in snapshot.Children)
                    {
                        // determinate if the username and password/mail are correct
                        if ( UserNameText.text == user.Child("username").Value.ToString() && PasswordText.text == user.Child("password").Value.ToString() ) {
                            exist = true;
                        }
                    }

                    if (exist) {
                    /* TODO: SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); */
                    SceneManager.LoadScene("Modos de juego");
                    }
                    else
                    {
                     SherUser.SetActive(true);
                    }

                    Debug.Log("user exist? " + exist);
                }
            });
    }
}
