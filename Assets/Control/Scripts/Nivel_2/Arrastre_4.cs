using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Arrastre_4 : MonoBehaviour
{    public GameObject Correcto, Continuar, casilla_1, casilla_2, Casilla_3, Casilla_4, Casilla_5, Casilla_6, Casilla_7, Casilla_8, Respuesta_1, Respuesta_2, Respuesta_3, Respuesta_4, Respuesta_5, Respuesta_6, Respuesta_7, Respuesta_8 , Respuesta_9;


    Vector3 initialRespuesta_6Position, initialRespuesta_2Position, initialRespuesta_3Position, initialRespuesta_1Position, initialRespuesta_5Position, initialRespuesta_4Position, initialRespuesta_7Position, initialRespuesta_8Position, initialRespuesta_9Position;

    bool Respuesta_6Bool, Respuesta_2Bool, Respuesta_1Bool, Respuesta_5Bool, Respuesta_7Bool , Respuesta_4Bool, Respuesta_8Bool, Respuesta_9Bool = false;

 

    void Start()
    {
        initialRespuesta_6Position = Respuesta_6.transform.position;
        initialRespuesta_2Position = Respuesta_2.transform.position;
        initialRespuesta_1Position = Respuesta_1.transform.position;
        initialRespuesta_5Position = Respuesta_5.transform.position;
        initialRespuesta_4Position = Respuesta_4.transform.position;
        initialRespuesta_7Position = Respuesta_7.transform.position;
        initialRespuesta_8Position = Respuesta_8.transform.position;
        initialRespuesta_9Position = Respuesta_9.transform.position;
        initialRespuesta_3Position = Respuesta_3.transform.position;
      

    }


 


    public void DragRespuesta_6()
    {

        
        Respuesta_6.transform.position = Input.mousePosition;

    }


    public void DragRespuesta_2()
    {

       
        Respuesta_2.transform.position = Input.mousePosition;

    }

    public void DragRespuesta_1()
    {

       
        Respuesta_1.transform.position = Input.mousePosition;

    }

    public void DragRespuesta_3()
    {

       
        Respuesta_3.transform.position = Input.mousePosition;

    }

    public void DragRespuesta_4()
    {

       
        Respuesta_4.transform.position = Input.mousePosition;

    }

    public void DragRespuesta_7()
    {

       
        Respuesta_7.transform.position = Input.mousePosition;

    }

    public void DragRespuesta_5()
    {

       
        Respuesta_5.transform.position = Input.mousePosition;

    }

    public void DragRespuesta_8()
    {

       
        Respuesta_8.transform.position = Input.mousePosition;

    }
    public void DragRespuesta_9()
    {

       
        Respuesta_9.transform.position = Input.mousePosition;

    }





    


    public void DropRespuesta_6()
    {

        float distance = Vector3.Distance(Respuesta_6.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_6.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_6.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_6.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_6.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_6.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_6.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_6.transform.position, Casilla_8.transform.position);

        if (distance_2 < 20)
        {
            Respuesta_6.transform.position = casilla_1.transform.position;
            Respuesta_6.transform.localScale = casilla_1.transform.localScale;
            Respuesta_6.GetComponent<Image>().color= new Color32(63,201,255,255);
            Respuesta_6Bool = true;
       
        }
        else if (distance_4 < 20)
        {
            Respuesta_6.transform.position = Casilla_5.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_5 < 20)
        {
            Respuesta_6.transform.position = Casilla_6.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_1 < 20)
        {
            Respuesta_6.transform.position = Casilla_3.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 20)
        {
            Respuesta_6.transform.position = casilla_2.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_6 < 20)
        {
            Respuesta_6.transform.position = Casilla_7.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_7 < 20)
        {
            Respuesta_6.transform.position = Casilla_8.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance < 20)
        {
            Respuesta_6.transform.position = Casilla_4.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_6.transform.position = initialRespuesta_6Position;
            Respuesta_6.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        




    }


    public void DropRespuesta_4()
    {

        float distance = Vector3.Distance(Respuesta_4.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_4.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_4.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_4.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_4.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_4.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_4.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_4.transform.position, Casilla_8.transform.position);

        if (distance_5 < 20)
        {
            Respuesta_4.transform.position = Casilla_6.transform.position;
            Respuesta_4.GetComponent<Image>().color= new Color32(250,17,17,200);
       
        }
        else if (distance_1 < 20)
        {
            Respuesta_4.transform.position = Casilla_3.transform.position;
            Respuesta_4.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 20)
        {
            Respuesta_4.transform.position = casilla_2.transform.position;
            Respuesta_4.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance < 20)
        {
            Respuesta_4.transform.position = Casilla_4.transform.position;
            Respuesta_4.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_4 < 20)
        {
            Respuesta_4.transform.position = Casilla_5.transform.position;
            Respuesta_4.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 20)
        {
            Respuesta_4.transform.position = casilla_1.transform.position;
            Respuesta_4.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_6 < 20)
        {
            Respuesta_4.transform.position = Casilla_7.transform.position;
            Respuesta_4.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_7 < 20)
        {
            Respuesta_4.transform.position = Casilla_8.transform.position;
            Respuesta_4.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_4.transform.position = initialRespuesta_4Position;
            Respuesta_4.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        




    }

    public void DropRespuesta_7()
    {

        float distance = Vector3.Distance(Respuesta_7.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_7.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_7.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_7.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_7.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_7.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_7.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_7.transform.position, Casilla_8.transform.position);
        


        if (distance_4 < 20)
        {
            Respuesta_7.transform.position = Casilla_5.transform.position;
            Respuesta_7.GetComponent<Image>().color= new Color32(250,17,17,200);
       
        }
        else if (distance_1 < 20)
        {
            Respuesta_7.transform.position = Casilla_3.transform.position;
            Respuesta_7.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 20)
        {
            Respuesta_7.transform.position = casilla_2.transform.position;
            Respuesta_7.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance < 20)
        {
            Respuesta_7.transform.position = Casilla_4.transform.position;
            Respuesta_7.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 20)
        {
            Respuesta_7.transform.position = casilla_1.transform.position;
            Respuesta_7.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_5 < 20)
        {
            Respuesta_7.transform.position = Casilla_6.transform.position;
            Respuesta_7.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_6 < 20)
        {
            Respuesta_7.transform.position = Casilla_7.transform.position;
            Respuesta_7.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_7 < 20)
        {
            Respuesta_7.transform.position = Casilla_8.transform.position;
            Respuesta_7.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_7.transform.position = initialRespuesta_7Position;
            Respuesta_7.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        




    }

    public void DropRespuesta_2()
    {

        float distance = Vector3.Distance(Respuesta_2.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_2.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_2.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_2.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_2.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_2.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_2.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_2.transform.position, Casilla_8.transform.position);


        if (distance_3 < 20)
        {
            Respuesta_2.transform.position = casilla_2.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);
           
        }
        else if (distance_1 < 20)
        {
            Respuesta_2.transform.position = Casilla_3.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance < 20)
        {
            Respuesta_2.transform.position = Casilla_4.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 20)
        {
            Respuesta_2.transform.position = casilla_1.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_4 < 20)
        {
            Respuesta_2.transform.position = Casilla_5.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_5 < 20)
        {
            Respuesta_2.transform.position = Casilla_6.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_6 < 20)
        {
            Respuesta_2.transform.position = Casilla_7.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_7 < 20)
        {
            Respuesta_2.transform.position = Casilla_8.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_2.transform.position = initialRespuesta_2Position;
            Respuesta_2.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        
    }

    public void DropRespuesta_1()
    {
        float distance = Vector3.Distance(Respuesta_1.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_1.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_1.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_1.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_1.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_1.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_1.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_1.transform.position, Casilla_8.transform.position);


        if (distance_1 < 20)
        {
            Respuesta_1.transform.position = Casilla_3.transform.position;
            Respuesta_1.transform.localScale = Casilla_3.transform.localScale;
            Respuesta_1.GetComponent<Image>().color= new Color32(63,201,255,255);
            Respuesta_1Bool = true;
          
        }
        else if (distance < 20)
        {
            Respuesta_1.transform.position = Casilla_4.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 20)
        {
            Respuesta_1.transform.position = casilla_2.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 20)
        {
            Respuesta_1.transform.position = casilla_1.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_4 < 20)
        {
            Respuesta_1.transform.position = Casilla_5.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_5 < 20)
        {
            Respuesta_1.transform.position = Casilla_6.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_6 < 20)
        {
            Respuesta_1.transform.position = Casilla_7.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_7 < 20)
        {
            Respuesta_1.transform.position = Casilla_8.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_1.transform.position = initialRespuesta_1Position;
            Respuesta_1.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        

    }


    public void DropRespuesta_5()
    {

        float distance = Vector3.Distance(Respuesta_5.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_5.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_5.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_5.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_5.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_5.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_5.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_5.transform.position, Casilla_8.transform.position);


        if (distance < 20)
        {
            Respuesta_5.transform.position = Casilla_4.transform.position;
            Respuesta_5.transform.localScale = Casilla_4.transform.localScale;
            Respuesta_5.GetComponent<Image>().color= new Color32(63,201,255,255);
            Respuesta_5Bool = true;

        }
        else if (distance_1 < 20)
        {
            Respuesta_5.transform.position = Casilla_3.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 20)
        {
            Respuesta_5.transform.position = casilla_2.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 20)
        {
            Respuesta_5.transform.position = casilla_1.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_4 < 20)
        {
            Respuesta_5.transform.position = Casilla_5.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_5 < 20)
        {
            Respuesta_5.transform.position = Casilla_6.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_6 < 20)
        {
            Respuesta_5.transform.position = Casilla_7.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_7 < 20)
        {
            Respuesta_5.transform.position = Casilla_8.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_5.transform.position = initialRespuesta_5Position;
            Respuesta_5.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        




    }

    public void DropRespuesta_8()
    {

        float distance = Vector3.Distance(Respuesta_8.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_8.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_8.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_8.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_8.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_8.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_8.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_8.transform.position, Casilla_8.transform.position);


        if (distance_7 < 20)
        {
            Respuesta_8.transform.position = Casilla_8.transform.position;
            Respuesta_8.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_1 < 20)
        {
            Respuesta_8.transform.position = Casilla_3.transform.position;
            Respuesta_8.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 20)
        {
            Respuesta_8.transform.position = casilla_2.transform.position;
            Respuesta_8.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 20)
        {
            Respuesta_8.transform.position = casilla_1.transform.position;
            Respuesta_8.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_4 < 20)
        {
            Respuesta_8.transform.position = Casilla_5.transform.position;
            Respuesta_8.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_5 < 20)
        {
            Respuesta_8.transform.position = Casilla_6.transform.position;
            Respuesta_8.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_6 < 20)
        {
            Respuesta_8.transform.position = Casilla_7.transform.position;
            Respuesta_8.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance < 20)
        {
            Respuesta_8.transform.position = Casilla_4.transform.position;
            Respuesta_8.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_8.transform.position = initialRespuesta_8Position;
            Respuesta_8.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        




    }

    public void DropRespuesta_9()
    {

        float distance = Vector3.Distance(Respuesta_9.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_9.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_9.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_9.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_9.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_9.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_9.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_9.transform.position, Casilla_8.transform.position);


        if (distance_6 < 20)
        {
            Respuesta_9.transform.position = Casilla_7.transform.position;
            Respuesta_9.transform.localScale = Casilla_7.transform.localScale;
            Respuesta_9.GetComponent<Image>().color= new Color32(63,201,255,255);
            Respuesta_9Bool = true;

        }
        else if (distance_1 < 20)
        {
            Respuesta_9.transform.position = Casilla_3.transform.position;
            Respuesta_9.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 20)
        {
            Respuesta_9.transform.position = casilla_2.transform.position;
            Respuesta_9.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 20)
        {
            Respuesta_9.transform.position = casilla_1.transform.position;
            Respuesta_9.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_4 < 20)
        {
            Respuesta_9.transform.position = Casilla_5.transform.position;
            Respuesta_9.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_5 < 20)
        {
            Respuesta_9.transform.position = Casilla_6.transform.position;
            Respuesta_9.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance < 20)
        {
            Respuesta_9.transform.position = Casilla_4.transform.position;
            Respuesta_9.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_7 < 20)
        {
            Respuesta_9.transform.position = Casilla_8.transform.position;
            Respuesta_9.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_9.transform.position = initialRespuesta_9Position;
            Respuesta_9.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        




    }

    public void DropRespuesta_3()
    {

        float distance = Vector3.Distance(Respuesta_3.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_3.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_3.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_3.transform.position, casilla_2.transform.position);
        float distance_4 = Vector3.Distance(Respuesta_3.transform.position, Casilla_5.transform.position);
        float distance_5 = Vector3.Distance(Respuesta_3.transform.position, Casilla_6.transform.position);
        float distance_6 = Vector3.Distance(Respuesta_3.transform.position, Casilla_7.transform.position);
        float distance_7 = Vector3.Distance(Respuesta_3.transform.position, Casilla_8.transform.position);


        if (distance_6 < 20)
        {
            Respuesta_3.transform.position = Casilla_7.transform.position;
            Respuesta_3.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_1 < 20)
        {
            Respuesta_3.transform.position = Casilla_3.transform.position;
            Respuesta_3.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 20)
        {
            Respuesta_3.transform.position = casilla_2.transform.position;
            Respuesta_3.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 20)
        {
            Respuesta_3.transform.position = casilla_1.transform.position;
            Respuesta_3.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_4 < 20)
        {
            Respuesta_3.transform.position = Casilla_5.transform.position;
            Respuesta_3.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_5 < 20)
        {
            Respuesta_3.transform.position = Casilla_6.transform.position;
            Respuesta_3.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance < 20)
        {
            Respuesta_3.transform.position = Casilla_4.transform.position;
            Respuesta_3.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_7 < 20)
        {
            Respuesta_3.transform.position = Casilla_8.transform.position;
            Respuesta_3.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_3.transform.position = initialRespuesta_3Position;
            Respuesta_3.GetComponent<Image>().color= new Color32(63,201,255,255);
        }
        




    }

     void Update()
    {
        if(Respuesta_6Bool && Respuesta_1Bool  && Respuesta_5Bool  && Respuesta_9Bool)
        {
            Continuar.SetActive(true);
            Correcto.SetActive(true);
           
        }
    }

  

}


