using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagmentScene : MonoBehaviour
{
    public static void CambiarEscena(string nombre)
    {

        SceneManager.LoadScene(nombre);
    }

    public static void addPuntos(float score)
    {
        TableroManager.instance.SetScore(score);
    }
}
