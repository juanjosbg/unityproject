﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorParser {
    //string colorEmbed;
    int fortuneSize;
    public Color[] parseColors(string colorEmbed)
    {
        // get the start and the end of the array
        int startArray = colorEmbed.IndexOf('[');
        int endArray = colorEmbed.IndexOf(']');

        // split the string to get every color
        string extractedColors = colorEmbed.Substring(startArray+1, endArray-startArray-1);
        string[] hexColors = extractedColors.Split(',');

        fortuneSize = hexColors.Length;


        Color[] colors = new Color[fortuneSize];
        for(int i = 0;i < hexColors.Length;++i)
        {
            hexColors[i] = hexColors[i].Trim('"');
            hexColors[i] = hexColors[i].Trim(' ');

            if(!hexColors[i].Contains("#"))
                hexColors[i] = hexColors[i].Insert(0, "#");

            Color newCol;
            if (ColorUtility.TryParseHtmlString(hexColors[i], out newCol))
            {
                colors[i] = newCol;
            }
        }

        return colors;
    }
    public int GetFortuneSize()
    {
        return fortuneSize;
    }
}
