using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Arrastre_1_1 : MonoBehaviour
{    public GameObject Correcto, Continuar, casilla_1, casilla_2, Casilla_3, Casilla_4, Respuesta_1, Respuesta_2, Respuesta_3, Respuesta_4, Respuesta_5, Respuesta_6;


    Vector3 initialRespuesta_6Position, initialRespuesta_2Position, initialRespuesta_1Position, initialRespuesta_5Position;

    bool Respuesta_6Bool, Respuesta_2Bool, Respuesta_1Bool, Respuesta_5Bool = false;



    void Start()
    {
        initialRespuesta_6Position = Respuesta_6.transform.position;
        initialRespuesta_2Position = Respuesta_2.transform.position;
        initialRespuesta_1Position = Respuesta_1.transform.position;
        initialRespuesta_5Position = Respuesta_5.transform.position;
      

    }


 


    public void DragRespuesta_6()
    {

        
        Respuesta_6.transform.position = Input.mousePosition;

    }


    public void DragRespuesta_2()
    {

       
        Respuesta_2.transform.position = Input.mousePosition;

    }

    public void DragRespuesta_1()
    {

       
        Respuesta_1.transform.position = Input.mousePosition;

    }
 
    public void DragRespuesta_5()
    {

       
        Respuesta_5.transform.position = Input.mousePosition;

    }





    


    public void DropRespuesta_6()
    {

        float distance = Vector3.Distance(Respuesta_6.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_6.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_6.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_6.transform.position, casilla_2.transform.position);



        if (distance_2 < 50)
        {
            Respuesta_6.transform.position = casilla_1.transform.position;
            Respuesta_6.transform.localScale = casilla_1.transform.localScale;
            Respuesta_6.GetComponent<Image>().color= new Color32(67,237,145,255);
            Respuesta_6Bool = true;

        }
        else if (distance_1 < 50)
        {
            Respuesta_6.transform.position = Casilla_3.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 50)
        {
            Respuesta_6.transform.position = casilla_2.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance < 50)
        {
            Respuesta_6.transform.position = Casilla_4.transform.position;
            Respuesta_6.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_6.transform.position = initialRespuesta_6Position;
            Respuesta_6.GetComponent<Image>().color= new Color32(67,237,145,255);
        }
        




    }

    public void DropRespuesta_2()
    {

        float distance = Vector3.Distance(Respuesta_2.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_2.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_2.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_2.transform.position, casilla_2.transform.position);



        if (distance_3 < 50)
        {
            Respuesta_2.transform.position = casilla_2.transform.position;
            Respuesta_2.transform.localScale = casilla_2.transform.localScale;
            Respuesta_2.GetComponent<Image>().color= new Color32(67,237,145,255);
            Respuesta_2Bool = true;

        }
        else if (distance_1 < 50)
        {
            Respuesta_2.transform.position = Casilla_3.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance < 50)
        {
            Respuesta_2.transform.position = Casilla_4.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 50)
        {
            Respuesta_2.transform.position = casilla_1.transform.position;
            Respuesta_2.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_2.transform.position = initialRespuesta_2Position;
            Respuesta_2.GetComponent<Image>().color= new Color32(67,237,145,255);
        }
        
    }

    public void DropRespuesta_1()
    {
        float distance = Vector3.Distance(Respuesta_1.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_1.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_1.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_1.transform.position, casilla_2.transform.position);



        if (distance_1 < 50)
        {
            Respuesta_1.transform.position = Casilla_3.transform.position;
            Respuesta_1.transform.localScale = Casilla_3.transform.localScale;
            Respuesta_1.GetComponent<Image>().color= new Color32(67,237,145,255);
            Respuesta_1Bool = true;
        }
        else if (distance < 50)
        {
            Respuesta_1.transform.position = Casilla_4.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 50)
        {
            Respuesta_1.transform.position = casilla_2.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 50)
        {
            Respuesta_1.transform.position = casilla_1.transform.position;
            Respuesta_1.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_1.transform.position = initialRespuesta_1Position;
            Respuesta_1.GetComponent<Image>().color= new Color32(67,237,145,255);
        }
        

    }


    public void DropRespuesta_5()
    {

        float distance = Vector3.Distance(Respuesta_5.transform.position, Casilla_4.transform.position);
        float distance_1 = Vector3.Distance(Respuesta_5.transform.position, Casilla_3.transform.position);
        float distance_2 = Vector3.Distance(Respuesta_5.transform.position, casilla_1.transform.position);
        float distance_3 = Vector3.Distance(Respuesta_5.transform.position, casilla_2.transform.position);



        if (distance < 50)
        {
            Respuesta_5.transform.position = Casilla_4.transform.position;
            Respuesta_5.transform.localScale = Casilla_4.transform.localScale;
            Respuesta_5.GetComponent<Image>().color= new Color32(67,237,145,255);
            Respuesta_5Bool = true;

        }
        else if (distance_1 < 50)
        {
            Respuesta_5.transform.position = Casilla_3.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);
        }
        else if (distance_3 < 50)
        {
            Respuesta_5.transform.position = casilla_2.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else if (distance_2 < 50)
        {
            Respuesta_5.transform.position = casilla_1.transform.position;
            Respuesta_5.GetComponent<Image>().color= new Color32(250,17,17,200);

        }
        else
        {
            Respuesta_5.transform.position = initialRespuesta_5Position;
            Respuesta_5.GetComponent<Image>().color= new Color32(67,237,145,255);
        }
        




    }

     void Update()
    {
        if(Respuesta_6Bool && Respuesta_1Bool && Respuesta_2Bool && Respuesta_5Bool)
        {
            Continuar.SetActive(true);
            Correcto.SetActive(true);
           
        }
    }

  
}


