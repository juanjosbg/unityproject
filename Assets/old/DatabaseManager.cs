﻿using Firebase;
using Firebase.Database;
using System.Collections;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class DatabaseManager : MonoBehaviour
{
	/*public InputField UserName;
	public InputField Mail;
	public InputField Password;*/
	public string userID;
	public DatabaseReference dbRef;

	public Text UserNameText;
	public Text PasswordText;
	public Text PasswordRepeat;
	//---------------------
	public Text NameUserText;
	public Text OldUserText;
	public Text CodeUserText;
	public Text GroupUserText;

	//---------------------
	public Toggle TogglePolitic;
	public Toggle ToggleEstudiante;
	public GameObject AlertRegister;
	public GameObject AlertCreate;
	//----------------------------------
	public bool stringDataEntered = false;
	public InputField DetectionStringEdad, DetectionStringCodigo, DetectionStringGroup;
	//------------------------------------
	/* Interfaces Estudiantes 0 - 1 */
	public GameObject TypeUserUI;
	public GameObject RegisterStudentUI;
	public GameObject RegisterStudent1UI;

	// Start is called before the first frame update
	void Start()
	{
		//string generatedUUID = GenerateUUID();
		//Debug.Log("Generated UUID: " + generatedUUID);

		userID = SystemInfo.deviceUniqueIdentifier;
		dbRef = FirebaseDatabase.DefaultInstance.RootReference;
	}

	public void CreateUser()
	{

		/* 		Debug.Log(UserNameText.text + PasswordText.text + PasswordRepeat.text + NameUserText.text + int.Parse(OldUserText.text) + int.Parse(CodeUserText.text) + int.Parse(GroupUserText.text));
		 */
		if (TogglePolitic.isOn)
		{
			Debug.Log("Creando...");
			User newuser = new User(UserNameText.text, PasswordText.text, PasswordRepeat.text, NameUserText.text, int.Parse(OldUserText.text), int.Parse(CodeUserText.text), int.Parse(GroupUserText.text));
			string json = JsonUtility.ToJson(newuser);
			Debug.Log(dbRef);
			dbRef.Child("users").Child(System.Guid.NewGuid().ToString()).SetRawJsonValueAsync(json);
			Debug.Log("USER CREATED!");

			AlertCreate.SetActive(true);
			RegisterStudent1UI.SetActive(false);
		}
		else
		{
			AlertRegister.SetActive(true);
		}
	}

	public void CheckdPassword()
	{
		bool isSame = PasswordRepeat.text == PasswordText.text;
		if (isSame)
		{
			CreateUser();
		}
		else
		{
			AlertRegister.SetActive(true);
		}
	}

	public void CheckTypeUsers()
	{
		bool isCheckStudents = ToggleEstudiante.isOn;
		if (isCheckStudents)
		{
			TypeUserUI.SetActive(false);
			RegisterStudentUI.SetActive(true);
		}
		else
		{
			TypeUserUI.SetActive(true);
			RegisterStudentUI.SetActive(false);
		}
	}

	public void ValidateUserInput()
	{
		// Obtén los valores de los campos
		string name = NameUserText.text;
		string oldUserText = OldUserText.text;
		string codeUserText = CodeUserText.text;
		string groupUserText = GroupUserText.text;
		bool togglePoliticIsOn = TogglePolitic.isOn;

		// Realiza las validaciones
		bool isValid = true; // Suponemos que los campos son válidos por defecto

		// Verificar si los campos no están vacíos
		if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(oldUserText) || string.IsNullOrEmpty(codeUserText) || string.IsNullOrEmpty(groupUserText))
		{
			isValid = false;
			
		}

		// Verificar si TogglePolitic está activado
		if (!togglePoliticIsOn)
		{
			isValid = false;
		}

		if (isValid)
		{
			RegisterStudentUI.SetActive(false);
			RegisterStudent1UI.SetActive(true);
		}
		else
		{
			// Alguno de los campos no es válido, muestra una alerta o realiza alguna acción
			AlertRegister.SetActive(true);
		}
	}

	public void OnInputFieldValueChanged(string newValue)
	{
		if (!string.IsNullOrEmpty(newValue))
		{
			DetectionStringEdad.onValueChanged.AddListener(OnInputFieldValueChanged);
			DetectionStringCodigo.onValueChanged.AddListener(OnInputFieldValueChanged);
			DetectionStringGroup.onValueChanged.AddListener(OnInputFieldValueChanged);

			stringDataEntered = true;
			ValidateUserInput();
			Debug.Log("Se esta revisando");
			AlertRegister.SetActive(true);
		}
		else
		{
			stringDataEntered = false;
			AlertRegister.SetActive(false);
		}
	}
}
