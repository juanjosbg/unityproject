using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuSeleccionPersonaje : MonoBehaviour
{

    private Sprite sprite;

    public void SeleccionarPersonaje()
    {
        sprite = GetComponent<Image>().sprite;
        GameManager.Instance.SetPersonaje(sprite);
        SceneManager.LoadScene("Historia");
    }

    public void IniciarJuego()

    {
        SceneManager.LoadScene("Tablero");
    }
}