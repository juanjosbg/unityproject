using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Movimiento_Arrastre : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public static GameObject itemDragginng;

    private Vector3 startPosition;
    private Transform startParent;
    private Transform itemDraggerParent;

    // Start is called before the first frame update
    void Start()
    {
        itemDraggerParent = GameObject.FindGameObjectWithTag("ItemDraggerParent").transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag"); 
        itemDragginng = gameObject; 

        startPosition = transform.position;
        startParent = transform.parent;
        transform.SetParent(itemDraggerParent);                  

    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag"); 
        transform.position = Input.mousePosition;
    }

     public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");

        itemDragginng = null;

        
        if (transform.parent == itemDraggerParent)
        {
            transform.position = startPosition;
            transform.SetParent(startParent);
        }
        
    }


}
