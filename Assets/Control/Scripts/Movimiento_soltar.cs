using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Movimiento_soltar : MonoBehaviour, IDropHandler
{

    public GameObject item;

    
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Drop");

        if (!item)
        {
            item = Movimiento_Arrastre.itemDragginng;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (item != null && item.transform.parent != transform)
        {
            Debug.Log("Remover");
            item = null;
        }
    }
}
