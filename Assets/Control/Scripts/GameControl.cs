using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    private static GameObject player;

    public static int numberToMove = 0;
    public static int player1StartWaypoint = 0;

    public GameObject ruleta;
    public GameObject botonGirar;
    public GameObject textoTurno;
    public GameObject casillaBlanca;

    public GameObject[] uiNivelSuperado;

    private AudioSource audioSource;




    public static bool gameOver = false;

    List<int> waypointIndices = new List<int> { 2, 5, 7, 16, 18, 20, 22, 25 };
    private void Awake()
    {
        ruleta.SetActive(false);
        botonGirar.SetActive(false);
        textoTurno.SetActive(false);
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        player = GameObject.Find("Player");
        player.GetComponent<FollowThePath>().moveAllowed = false;
        nivelCompletado();
    }

    // Update is called once per frame
    void Update()
    {

        player.GetComponent<FollowThePath>().futureWaypointIndex = player1StartWaypoint + numberToMove;

        if ((player.GetComponent<FollowThePath>().waypointIndex > player1StartWaypoint + numberToMove))
        {
            player.GetComponent<FollowThePath>().futureWaypointIndex = player1StartWaypoint + numberToMove;
            player1StartWaypoint = player.GetComponent<FollowThePath>().waypointIndex - 1;
            player.GetComponent<FollowThePath>().moveAllowed = false;
            TableroManager.instance.SetWaypointIndex(player1StartWaypoint);
            textoTurno.SetActive(false);
            if (waypointIndices.Contains(player.GetComponent<FollowThePath>().futureWaypointIndex))
            {
                Invoke("ActivarCasillaBlanca", 1f);

            }
            else
            {
                Invoke("EjecutarEscena", 3f);
            }

        }

        if (player.GetComponent<FollowThePath>().waypointIndex == player.GetComponent<FollowThePath>().waypoints.Length)
        {
            gameOver = true;
            Invoke("GameOver", 1.5f);
        }

    }


    void EjecutarEscena()
    {
        int futureWaypointIndex = player1StartWaypoint + numberToMove;

        switch (futureWaypointIndex)
        {
            case int n when (n > 0 && n < 8):
                CargarEscenaAleatoria("Nivel_1");
                break;
            case int n when (n >= 8 && n < 16):
                CargarEscenaAleatoria("Nivel_2");
                break;
            case int n when (n >= 16 && n < 24):
                CargarEscenaAleatoria("Nivel_3");
                break;
            case int n when (n >= 24 && n < 31):
                CargarEscenaAleatoria("Nivel_4");
                break;
            default:
                Debug.LogError("El índice del punto de referencia futuro no está en ningún rango de niveles.");
                break;
        }
    }
    void CargarEscenaAleatoria(string nombreCarpeta)
    {
        string[] escenas = ObtenerEscenasEnCarpeta(nombreCarpeta);
        if (escenas.Length > 0)
        {
            string escenaAleatoria = escenas[Random.Range(0, escenas.Length)];
            SceneManager.LoadScene(escenaAleatoria);
        }
    }
    string[] ObtenerEscenasEnCarpeta(string carpeta)
    {
        List<string> escenas = new List<string>();
        string[] escenasEnProyecto = System.IO.Directory.GetFiles("Assets/", "*.unity", System.IO.SearchOption.AllDirectories);

        foreach (string escenaPath in escenasEnProyecto)
        {
            if (escenaPath.Contains(carpeta))
            {
                escenas.Add(System.IO.Path.GetFileNameWithoutExtension(escenaPath));
            }
        }

        return escenas.ToArray();
    }

    void IniciaPartida()
    {
        Invoke("ActivarTextoTurno", 1.5f);
        Invoke("ActivarRuleta", 3f);
    }

    void nivelCompletado()
    {
        if (player1StartWaypoint >= 8 && player1StartWaypoint < 16 && !TableroManager.instance.GetPrincipiante())
        {
            uiNivelSuperado[0].SetActive(true);
            TableroManager.instance.SetPrincipiante(true);
            StartCoroutine(nivelCompletadoUI(uiNivelSuperado[0]));
            Invoke("IniciaPartida", 3f);
        }
        else if (player1StartWaypoint >= 16 && player1StartWaypoint < 24 && !TableroManager.instance.GetIntermedio())
        {
            uiNivelSuperado[1].SetActive(true);
            TableroManager.instance.SetIntermedio(true);
            StartCoroutine(nivelCompletadoUI(uiNivelSuperado[1]));
            Invoke("IniciaPartida", 3f);
        }
        else if (player1StartWaypoint >= 24 && !TableroManager.instance.GetAvanzado())
        {
            uiNivelSuperado[2].SetActive(true);
            TableroManager.instance.SetAvanzado(true);
            StartCoroutine(nivelCompletadoUI(uiNivelSuperado[2]));
            Invoke("IniciaPartida", 3f);
        }
        else
        {
            IniciaPartida();

        }

    }
    IEnumerator nivelCompletadoUI(GameObject ui)
    {
        yield return new WaitForSeconds(3f);
        ui.SetActive(false);
    }
    public static void MovePlayer(int playerToMove)
    {
        if (playerToMove == 1)
        {
            player.GetComponent<FollowThePath>().moveAllowed = true;
        }
    }
    void SceneTest()
    {

    }
    void ActivarRuleta()
    {
        // Activa el objeto de Canvas
        ruleta.SetActive(true);
        botonGirar.SetActive(true);
    }

    void ActivarTextoTurno()
    {
        // Activa el objeto de Canvas
        textoTurno.SetActive(true);
        audioSource.PlayOneShot(audioSource.clip);
    }

    void DesactivarRuleta()
    {
        // Activa el objeto de Canvas
        ruleta.SetActive(false);
    }
    void DesactivarBoton()
    {
        // Activa el objeto de Canvas
        botonGirar.SetActive(false);
    }

    void DesactivarTextoTurno()
    {
        // Activa el objeto de Canvas
        textoTurno.SetActive(false);
    }

    void ActivarCasillaBlanca()
    {
        casillaBlanca.SetActive(true);
    }

    public void DesactivarCasillaBlanca()
    {
        casillaBlanca.SetActive(false);
        Invoke("ActivarTextoTurno", 1f);
        Invoke("ActivarRuleta", 3f);
    }

    void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
}
