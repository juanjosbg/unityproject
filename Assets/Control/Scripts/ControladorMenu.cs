using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ControladorMenu : MonoBehaviour
{
    public void NewGame()
    {
        SceneManager.LoadScene("Modos de juego");

    }

    public void Quit()
    {
        Debug.Log("Juego Cerrado");
        Application.Quit();
        
    }
}