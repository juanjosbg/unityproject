using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableroManager : MonoBehaviour
{
    public static TableroManager instance;

    [SerializeField] private float score;

    [SerializeField] private int waypointIndex;

    [SerializeField] private bool principiante = false;
    [SerializeField] private bool intermedio = false;
    [SerializeField] private bool avanzado = false;
    [SerializeField] private bool experto = false;


    private void Awake()
    {
        if (TableroManager.instance == null)
        {
            TableroManager.instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void SetScore(float scorePoints)
    {
        score += scorePoints;
    }

    public float GetScore()
    {
        return score;
    }

    public void SetWaypointIndex(int point)
    {
        waypointIndex = point;
    }

    public int GetWaypointIndex()
    {
        return waypointIndex;
    }

    public void SetPrincipiante(bool value)
    {
        principiante = value;
    }

    public bool GetPrincipiante()
    {
        return principiante;
    }

    public void SetIntermedio(bool value)
    {
        intermedio = value;
    }

    public bool GetIntermedio()
    {
        return intermedio;
    }

    public void SetAvanzado(bool value)
    {
        avanzado = value;
    }

    public bool GetAvanzado()
    {
        return avanzado;
    }

    public void SetExperto(bool value)
    {
        experto = value;
    }

    public bool GetExperto()
    {
        return experto;
    }
}
