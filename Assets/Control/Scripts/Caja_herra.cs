using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Caja_herra : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        if (Movimiento_Arrastre.itemDragginng == null) return;
        Movimiento_Arrastre.itemDragginng.transform.SetParent(transform);
    }
}
