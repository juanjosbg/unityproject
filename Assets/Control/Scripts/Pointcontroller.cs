using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Pointcontroller : MonoBehaviour
{
    // Start is called before the first frame update
    private TextMeshProUGUI text;
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = TableroManager.instance.GetScore().ToString();
    }
}
