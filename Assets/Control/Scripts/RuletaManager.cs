using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuletaManager : MonoBehaviour
{

    private int whosTurn = 1;
    private bool spinAllowed = true;
    public FortuneWheel fortuneWheel;

    public GameObject ruleta;
    public GameObject botonGirar;
    public GameObject textoTurno;

    public void GirarRuleta()
    {
        if (!GameControl.gameOver && spinAllowed)
        {
            DesactivarBoton();
            StartCoroutine(GirarRuletaCoroutine());
        }

    }


    IEnumerator GirarRuletaCoroutine()
    {
        spinAllowed = false;
        yield return StartCoroutine(fortuneWheel.StartFortune());
        GameControl.numberToMove = int.Parse(fortuneWheel.GetLatestResult());
        GameControl.MovePlayer(whosTurn);
        spinAllowed = true;
        Invoke("DesactivarRuleta", 2f);
    }

    void DesactivarRuleta()
    {
        // Activa el objeto de Canvas
        ruleta.SetActive(false);
    }
    void DesactivarBoton()
    {
        // Activa el objeto de Canvas
        botonGirar.SetActive(false);
    }

    void DesactivarTextoTurno()
    {
        // Activa el objeto de Canvas
        textoTurno.SetActive(false);
    }
}
