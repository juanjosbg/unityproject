# ProyectoUnity
----
#### Temas a tratar

## Tutoriales -
- los temas son relacionados a **UNITY**
    * Tutorial de FIREBASE con UNITY [Autenticación del usuario y setup](https://www.youtube.com/watch?v=OvkFsAtMGVY&list=PLAGy_slICtV2zuunVCpxJe1NFSc3YgkSk "Video link")
    
    * Tutorial de Guardar y cargar datos desde Firestore
      [pODER Guardar y cargar datos EN UNITY con firestore](https://www.youtube.com/watch?v=vs0YtpsA2Js&list=PLAGy_slICtV2zuunVCpxJe1NFSc3YgkSk&index=2 "Video link")
    
    * Tutorial multijugador con UNITY [MULTIJUGADOR CON FIREBASE](https://www.youtube.com/watch?v=iYUXOahb3I0 "Video link")

    * Tutorial Authentication Firebase [Authentication in Unity](https://www.youtube.com/watch?v=XxzPdCspDUU "Video link")
 
    * Unity Login System with Firebase Authentication [Simple Login & Signup Interface | Part 1/4](https://www.youtube.com/watch?v=V143hJru1-g&list=PLQBhQREjWwvBGETZRZJYJVqZVbdBlTyk- "Video link")

----
### Tener en cuenta 
> Se esta trabajando para **MOVILE**
  - Medidas Movile (1920 x 1080)

----
### Falta
* El paquete para subir el videojuego a Google Play Game
