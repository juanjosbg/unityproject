﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class FortuneElement
{
    public string label;
    public Color labelColor;

    [Range(4, 10)]
    public float fontSize;

    public Color fillColor;
    
    public Sprite icon;
    public bool showLabelInWheel = true;
    public FortuneElement()
    {
        this.label = "New Slice";
        this.labelColor =  new Color(0, 0, 0, 1);
        this.fontSize = 5;
        this.fillColor = new Color(1, 1, 1, 1);
    }

}
