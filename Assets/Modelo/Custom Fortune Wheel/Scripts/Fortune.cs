﻿using System.Collections;
using UnityEngine;


public abstract class Fortune : MonoBehaviour
{
    string[] _labels;
    // protected Color[] _colors;
    /// <summary> If you want to double the instances number of the elements to get the selection chance doubled. </summary>
    public bool doubleChance = false;
    ///<summary>
    /// Instantiate the fortune wheel elements. 
    /// A sub-gameobject is created for the elements.
    ///</summary>
    public abstract void CreateFortune();


    /// <summary>
    /// Start rotating the wheel.
    /// <br/>
    /// This function works in a spearated thread so call it in a coroutine.
    /// <br/>
    /// <example> Exmaple:
    /// <code>
    ///
    ///    IEnumerator CoroutineExamle()
    ///    { 
    ///        <br/>
    ///        yield return StartCoroutine( StartFortune() );
    ///        <br/>
    ///    }
    /// </code>
    /// </example>
    /// </summary>  
    public abstract IEnumerator StartFortune();
    /// <summary>
    /// Get the label result of the latest fortune spin.
    /// <br/>
    /// You can use if after you make at least one spin.
    /// </summary>
    /// <returns>
    /// The label of the result.
    /// </returns>
    public abstract string GetLatestResult();
    /// <summary>
    /// Get the ID result of the latest fortune spin.
    /// <br/>
    /// You can use if after you make at least one spin.
    /// </summary>
    /// <returns>
    /// The ID of the result.
    /// </returns>
    public abstract int GetLatestResultID();
    
    //public abstract object GetResult();
}
